import { useState, useEffect } from 'react'

const useFetch = <T> (url: string): T => {
  const [data, setData] = useState<T>((null as unknown) as T)

  useEffect(() => {

    fetch(url)
      .then(res => res.json())
      .then(resData => setData(resData))
  }, [url])

  return data
}

export default useFetch
