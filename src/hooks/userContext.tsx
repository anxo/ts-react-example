import React, { createContext, useContext, useState } from 'react'

export interface User {
  name: string
}

const UserContext = createContext<[User | undefined, (user: User) => void]>(null as any)

interface UserProviderProps {
  children: JSX.Element
}


export const UserProvider = ({ children }: UserProviderProps): JSX.Element => {
  const [user, setUser] = useState<User>()
  return (
    <UserContext.Provider value={[user, setUser]}>
      {children}
    </UserContext.Provider>
  )
}

export const useUser = () => useContext(UserContext)?.[0]
