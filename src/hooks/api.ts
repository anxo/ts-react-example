import { SearchParams } from "../routes/Search/Search";
import useFetch from "./useFetch";
import { User } from "./userContext";

interface Character {
  id: number
  name: string
  image: string
}

interface SearchResult<T> {
  info: {
    count: number
    next?: number
    prev?: number
    pages: number
  }
  results: T[]
  isError: false
}

interface ApiError {
  isError: true
  message: string
}

type ApiResult<T> = ApiError | SearchResult<T>

export const useCharacterSearch = (searchParams: SearchParams): ApiResult<Character> | null =>
  useFetch('https://rickandmortyapi.com/api/character?' + new URLSearchParams(searchParams as any).toString())

/*
interface UserDetails {
  name: string
  isActive: boolean
}

const user: UserDetails = { name: 'Manolo', isActive: true }

function saluda<T extends User>(user: T): T {
  console.log('Hola,', user.name)
  return user
}

const user2 = saluda(user)
*/
