import React from 'react';
import { useCharacterSearch } from '../../hooks/api';
import { SearchParams } from './Search';

interface SearchResultsProps {
  searchParams: SearchParams
}

function SearchResults({ searchParams }: SearchResultsProps) {
  const data = useCharacterSearch(searchParams)

  if (data?.isError) {
    throw new Error(data?.message)
  }

  return (
    <div className="results">
      {data?.results?.map((result) =>
        <div className="result" key={result.id}>
          <img src={result.image} alt={result.name}/>
          <span>{result.name}</span>
        </div>
      )}
    </div>
  );
}

export default SearchResults;
