import React, { useState } from 'react';
import SearchForm from './SearchForm';
import SearchResults from './SearchResults';
import ErrorBoundary from '../../components/ErrorBoundary/ErrorBoundary';
import './Search.css'

export interface SearchParams {
  name?: string
  gender?: string
}

function Search() {
  const [searchParams, setSearchParams] = useState<SearchParams>()
  return (
    <div className="page search">
      <h1>Buscador</h1>
      <SearchForm setSearchParams={setSearchParams}/>
      {searchParams &&
        <ErrorBoundary>
          <SearchResults searchParams={searchParams} />
        </ErrorBoundary>
      }
    </div>
  );
}

export default Search;
