import React, { useState } from 'react';
import { SearchParams } from './Search';

interface SearchFormProps {
  setSearchParams: (searchParams: SearchParams) => void
}

function SearchForm({ setSearchParams }: SearchFormProps): JSX.Element {
  const [name, setName] = useState<string>('')
  const [gender, setGender] = useState<string>('')

  const handleSubmit: React.FormEventHandler = (e) => {
    e.preventDefault()
    setSearchParams({ name, gender })
  }

  return (
    <form className="search-form" onSubmit={handleSubmit}>
      <label>
        <span>Nombre:</span>
        <input value={name} onChange={e => setName(e.target.value)} />
      </label>
      <label>
        <span>Género:</span>
        <select value={gender} onChange={e => setGender(e.target.value)}>
          <option value="">Cualquiera</option>
          <option value="male">Macho</option>
          <option value="female">Hembra</option>
          <option value="genderless">Sin género</option>
          <option value="unknown">Desconocido</option>
        </select>
      </label>
      <button>Buscar</button>
    </form>
  );
}

export default SearchForm;
