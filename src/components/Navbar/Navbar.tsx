import React from 'react';
import { NavLink } from 'react-router-dom';
import { useUser } from '../../hooks/userContext';
import './Navbar.css';

function Navbar() {
  const user = useUser()

  return (
    <header className="navbar">
      <span>TS Demo</span>
      <span>
        <NavLink to="/">Inicio</NavLink>
        <NavLink to="/search">Buscador</NavLink>
      </span>
      <span>{user ? user.name : 'Anonymous'}</span>
    </header>
  );
}

export default Navbar;
