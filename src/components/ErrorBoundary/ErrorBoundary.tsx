import React, { Component } from "react"

class ErrorBoundary extends Component<{ children: any }, { hasError: boolean }> {
  public state = {
    hasError: false
  }

  public static getDerivedStateFromError(_: any) {
    return { hasError: true }
  }

  public componentDidCatch(error: any, errorInfo: any) {
    console.error("Uncaught error:", error, errorInfo)
  }

  public render() {
    if (this.state.hasError) {
      return <h1>Sorry.. there was an error</h1>
    }

    return this.props.children
  }
}

export default ErrorBoundary
