# TS React Example

Un ejemplo de aplicación React básica con Typescript.

## Uso

Al bajarse el repositorio, seguiremos los pasos habituales:

```sh
npm install
npm start
```

## Enlaces

Tienes disponible la [presentación](https://docs.google.com/presentation/d/1rOpJYDE6bO4Gmqh2iBZgohlYZzovx16-NoITR8U-Jjc/edit?usp=sharing) usada al principio de la charla, y el repositorio correspondiente al [ejemplo con Node](https://gitlab.com/anxo/ts-node-example).
